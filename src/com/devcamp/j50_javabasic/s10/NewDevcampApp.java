package com.devcamp.j50_javabasic.s10;

public class NewDevcampApp {
    public static void main(String[] args) {
        System.out.println("I'm a new Java Developer!");

        //Sử dụng phương thức thường => gắn đối tượng (khai báo Lớp biến)
        NewDevcampApp app = new NewDevcampApp();
        String myName = app.getName();
        app.name(myName);

        //Sử dụng phương thức static => gắn trực tiếp với tên lớp
        NewDevcampApp.name(myName, 40);
    }

    public void name(String fullname) {
        System.out.println("My name is " + fullname);
    }

    public String getName() {
        return "Le Tuan";
    }

    public static void name(String fullname, int age) {
        System.out.println("My name is " + fullname + ", age is " + age);
    }  
}
