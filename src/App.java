import com.devcamp.j50_javabasic.s10.NewDevcampApp;

public class App {
    /*
        Lớp App demo
        Hàm main khởi chạy chương trình
    */
    public static void main(String[] args) throws Exception {
        //Khai báo biên String
        String appName = "Devcamp";

        System.out.println("Hello: " + appName);
        System.out.println("Length: " + appName.length());
        System.out.println("ToLowerCase: " + appName.toLowerCase());
        System.out.println("ToUperCase: " + appName.toUpperCase());

        //Thao tác với lớp NewDevcampApp
        NewDevcampApp app = new NewDevcampApp();
        String myName = app.getName();
        app.name(myName);

        NewDevcampApp.name(myName, 42);
    }
}
